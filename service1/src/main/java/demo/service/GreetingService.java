package demo.service;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class GreetingService {
    private final RestTemplate restTemplate = new RestTemplate();

    public String getData() {
        String url = "http://localhost:8082/test2";
        return restTemplate.getForObject(url, String.class);
    }
}
