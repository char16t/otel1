```
docker run -d --name jaeger \
  -e COLLECTOR_ZIPKIN_HTTP_PORT=9411 \
  -p 5775:5775/udp \
  -p 6831:6831/udp \
  -p 6832:6832/udp \
  -p 5778:5778 \
  -p 16686:16686 \
  -p 14268:14268 \
  -p 9411:9411 \
  jaegertracing/all-in-one:1.6
```

UI here: http://localhost:16686/search

```
mvn clean package
export OTEL_EXPORTER=jaeger
export OTEL_METRICS_EXPORTER=none
export OTEL_EXPORTER_JAEGER_ENDPOINT=http://localhost:14268/api/traces
```

```
java -javaagent:../opentelemetry-javaagent.jar \
    -Dotel.resource.attributes=service.name=service2 \
    -Dotel.traces.exporter=zipkin \
    -jar target/demo2-0.0.1-SNAPSHOT.jar
```